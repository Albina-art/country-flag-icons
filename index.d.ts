export as namespace CountryFlagIcons;


export interface Module {}

export function hasFlag(countryCode: string): boolean;
